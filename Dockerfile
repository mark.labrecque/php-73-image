FROM php:7.3-apache

RUN apt-get update && apt-get install -y \
      libfreetype6-dev \
      libjpeg62-turbo-dev \
      libpng-dev \
      wget \
      git \
      zip \
      vim \
      default-mysql-client

RUN docker-php-ext-install gd \
    && docker-php-ext-install opcache pdo_mysql

COPY docker/apache/vhost.conf /etc/apache2/sites-available/000-default.conf
# php.ini is intentionally empty for now
COPY docker/php/php.ini /usr/local/etc/php/php.ini

RUN a2enmod rewrite

WORKDIR /tmp
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \ 
    php composer-setup.php --filename=composer --install-dir=/usr/local/bin && \
    composer --version && \
    rm composer-setup.php

WORKDIR /var/www